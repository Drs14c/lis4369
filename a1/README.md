> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4369

## DeVon Singleton

### Assignment 1 Requirements:

*Course Work Link*

 [A1 README.md](a1/README.md)

1. Distributed Version Control with Git and Bitbucket
2. Development Installation
3. Questions
4. Bitbucket repo links
#### README.md file should include the following items:


* Screenshot of a1 tip calculator application running 
* Git commands with short description


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create a new local repository
2. git status - list the files you have changed and those you still need to add or commit
3. git add - add one or more files to staging(index)
4. git commit - commit changes to head (but not yet the remote repoistory) 
5. git push - send changes to the master branch of your remote repository
6. git pull - fetch and merged changes on the remote server to your working directory
7. git diff - view all the merge conflicts 
 

#### Assignment Screenshots:



![Tip Calculator Screenshot](img/tip_cal_vs.png)

test

![Tip Calculator Screenshot](img/tip_cal_idle.png)

*Bit Bucket Tutorial*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
