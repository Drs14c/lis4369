>    - NOTE:   -  This README.md file should be placed at the - root of each of your repos directories.      - 
>
>Also, this file  - must -  use Markdown syntax, and provide project documentation as per below - otherwise, points  - will  -  be deducted.
>

# Lis4369

## DeVon Singleton

### LIS4369 # Requirements:

  -Course Work Link      - 

 
 
 
 
1.  [A1 README.md](a1/README.md)

    - Install Python

      - Install R

      - Install R Studio

      - Install Visual Studio Code

      - Create a1_tip_calculator application

      - Create a Bitbucket repo

      - Provide git command descriptions


2. [A2 README.md](a2/README.md)

      - Bruschetta Recipe Application  

      - Provide screenshot of Bruschetta Application



3. [A3 README.md](a3/README.md)
	
     - Used iteration structure (aka loop). 
    
     - Formatted, right aligned numbers, and rounded to two decimal places. 
    
     - Created at least three functions that are called by the program
    
	      - a. main(): calls at least two other functions. 
    
	      - b. get_requirements(): displays program requirements. 
    
	      - c.estimate_painting_cost(): calculates interior home painting. 


4. [A4 README.md](a4/README.md)
	
     - Installed all needed packages	 
	
     - Screenshot of Data Analysis graph
	
     - Created at least three functions that are called by the program

	      - a. main(): calls at least two other functions that are called by the program.
        
	      - b. get_requirements(): displays the program requirements.
        
	      - c. data_analysis(): displays the following data.


5. [A5 README.md](a5/README.md)
	
     - Complete the following tutorial : Introduction to r setup
	
     - Code and run lis4369_a5.r
	
     - Include two plots

6. [P1 README.md](p1/README.md)

     - Installed all needed packages 
	
     - Screenshot of Data Analysis graph
	
     - Created at least three functions that are called by the program

	      - a. main(): calls at least two other functions that are called by the program.
        
	      - b. get_requirements(): displays the program requirements.
        
	      - c. data_analysis(): displays the following data. 

7. [P2 README.md](p2/README.md)

     - Use Assignment 5 screenshots and references above for the following requirements:

     - Backward      - engineer the lis4369_p2_requirements.txtfile

     - Be sure to include at least two plots in your README.md file

	      - a. Using qplot

	      - b. Using plot 


