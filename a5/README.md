> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4369

## DeVon Singleton

### Assignment 5 Requirements:

*Course Work Link*

 
1. Complete R setup
2. Install proper packages
3. Code and Run lis4369_a5.r
4. Questions
#### README.md file should include the following items:


* Screenshot of Graphs


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>


 

#### Assignment Screenshots:


###Screenshot from tutorial:

![Screenshot of tutorial](img/overall.png)

![Screenshot of tutorial plot1](img/1.png)

![Screenshot of tutorial plot2](img/2.png)

![Screenshot of tutorial plot3](img/3.png)

![Screenshot of tutorial plot4](img/4.png)

![Screenshot of tutorial plot5](img/5.png)


###Screenshot from A5:
![Screenshot of A5 plot1](img/r1.png)

![Screenshot of A5 plot2](img/r2.png)



*Bit Bucket Tutorial*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
