> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4369

## DeVon Singleton

### Assignment 2 Requirements:

*Course Work Link*

 [A2 README.md](a2/README.md)

1. Distributed Version Control with Git and Bitbucket
2. Screenshot of Payroll Calculator
3. Questions
4. Bitbucket repo links
#### README.md file should include the following items:


* Screenshot of Payroll Calculator application running 
* Git commands with short description


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>


 

#### Assignment Screenshots:



![Payroll Calculator with Overtime](img/payroll_calculator_ot_idle.png)



![Payroll Calculator without Overtime](img/payroll_calculator_vs.png)

*Bit Bucket Tutorial*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
