url = "http://vincentarelbundock.github.io/Rdatasets/csv/datasets/mtcars.csv"
mtcars <- read.csv(file=url,head=TRUE,sep=",") #read file, and assigns to variable

cat("*** Report/Command Requirements ***","\n") 

cat("(1) Display all data from file:","\n")
mtcars

cat("\n","(2) Display 1st 10 records:","\n")
head(mtcars, 10)

cat("\n","(3) Display last 10 records:","\n")
tail(mtcars, 10)

cat("\n","(4) Display file structure(see notes above):","\n")
str(mtcars)

cat("\n","(5) Display column names(see notes above):","\n")
names(mtcars)

cat("\n","(6) Display 1st record/row with column names(see notes above):","\n")
mtcars[1,]

cat("\n","(7) Display 2nd column data (mpg), using column number:","\n" ,
"Note: Does not include column name (see notes above)","\n")
mtcars[,2]

cat("\n","(8) Display column data (cyl), using column name:","\n" ,
    "Note: Return does not display column name (see notes above)","\n")
mtcars$cyl

cat("\n","(9) Display row/column data (3,4), that is, one field ,using square bracket notation:","\n" ,
    "Note: Does not include column name (see notes above)","\n")
mtcars[3,4]

cat("\n","(10) Display all data for cars having greater than 4 cylinders:","\n")
mtcars[mtcars$cyl>4,]

#Note: using attach function, dont need to write mtcars$ every time
#attach(mtcars) be careful when using attach
#mtcars[cyl>4] #same as line above

cat("\n","(11) Display all cars having greater than 4 cylinders and greater than 4 gears:","\n")
mtcars[mtcars$cyl>4 &mtcars$gear>4,] # and = &
#simple notation using attach() be careful 
#mtcars[cyl>4 &gear>4,] 

cat("\n","(12) Display all cars having greater than 4 cylinders and exactly 4 gears:","\n")
mtcars[mtcars$cyl>4 &mtcars$gear==4,] # equality equal == 
#simple notation using attach() be careful 
#mtcars[cyl>4 &gear==4,]

cat("\n","(13) Display all cars having greater than 4 cylinders or exactly than 4 gears:","\n")
mtcars[mtcars$cyl>4 |mtcars$gear==4,] # equality equal == 
#simple notation using attach() be careful 
#mtcars[cyl>4 &gear>4,]

cat("\n","(14) Display all cars having greater than 4 cylinders and do not have 4 gears:","\n")
mtcars[mtcars$cyl>4 |mtcars$gear!=4,] # not equal !=

cat("\n","(15) Display total number of rows (only the numbers):","\n")
nrow(mtcars)

cat("\n","(16) Display total number of columns (only the numbers):","\n")
ncol(mtcars)

cat("\n","(17) Display total number of dimensions (i.e., rows and columns) :","\n")
dim(mtcars)#dimensions = row and columns

cat("\n","(18) Display data frame structure - same as info in python:","\n")
str(mtcars)

cat("\n","(19) Get mean, median, minimum, maximum, quantiles, variance and standard deviation of horsepower (Note: remove any missing data):","\n")

cat("\t", "a. Mean")
mean(mtcars$hp, na.rm=TRUE)

cat("\t", "b. Median")
median(mtcars$hp, na.rm=TRUE)

cat("\t", "c. Min")
min(mtcars$hp, na.rm=TRUE)

cat("\t", "d. Max")
max(mtcars$hp, na.rm=TRUE)

cat("\t", "e. Quantile")
quantile(mtcars$hp, na.rm=TRUE)

cat("\t", "f. Variance")
var(mtcars$hp, na.rm=TRUE)

cat("\t", "g. Standard DeViation")
sd(mtcars$hp, na.rm=TRUE)

cat("\n","(20) summary() function prints min, max, median, and quantiles (also number of N/A if any):","\n")
summary(mtcars$hp, na.rm=TRUE)

cat("\n", "Two plots (must include your name in title ): 1. uses qplot(); 2. uses plot();", "\n" )
library(ggplot2)
#Graphics with ggplot2: https://www.statmethods.net//advgraphs/ggplot2.html
#output graph to .png file ; https://www.statmethods.net/interface/io.html
png("plot_disp_and_mpg_1.png") # to cwd
qplot(disp, mpg, data=mtcars,
      xlab = "Displacement",
      ylab = "MPG",
      colour = cyl,
      main="DeVon Singleton: Displacement vs MPG")
dev.off()

png("plot_disp_and_mpg_2.png") # to cwd
plot(mtcars$wt, mtcars$mpg, 
      xlab = "Weight in Thousands",
      ylab = "MPG",
      main="DeVon Singleton: Weight vs MPG", 
     las=1)

dev.off()

#release file
sink.reset <- function(){
  for(i in seq_len(sink.number())){
    sink(NULL)
  }
}
  #call UDF:
sink.reset()
