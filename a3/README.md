> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Lis4369

## DeVon Singleton

### Assignment 3 Requirements:

*Course Work Link*

 
1. Distributed Version Control with Git and Bitbucket
2. Screenshot of Payroll Calculator
3. Questions
4. Bitbucket repo links
#### README.md file should include the following items:


* Screenshot of Painting Estimator application running 


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>


 

#### Assignment Screenshots:



![Painting Estimator](img/painting_estimator.png)



![Painting Estimator Continue](img/painting_estimator2.png)

*Bit Bucket Tutorial*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Drs14c/bitbucketstationslocations/src/master/)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
