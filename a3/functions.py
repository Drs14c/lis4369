#!/usr/bin/env python3


def get_requirements():
    print("Painting Estimator")
    print("\nProgram Requirements: "
        
        + "\n1. Calculate home interior paint cost (w/o primer). \n"
        + "2. Must use float data type.\n"
        + "3. Must use SQFT_PER_GALLON constant (350). \n"
        + "4. Must use iteration structure (aka loop). \n"
        + "5. Format, right align numbers, and round to two decimal places. \n"
        + "6. Create at least three functions that are called by the program: \n"
        + "\ta. main(): calls at least two other functions. \n"
        + "\tb. get_requirements(): displays program requirements. \n"
        + "\tc. estimate_painting_cost(): calculates interior home painting. \n")

def estimate_painting_cost():
    #initialize variables
    SQFT_PER_GALLON = 350
    total_sqft = 0.0

    #get user data
    print("Input ")
    total_sqft = float(input("Enter total interior sq ft: "))
    price_per_gallon = float(input("Enter price per gallon paint: "))
    painting_rate = float(input("Enter hourly painting rate per sqft: "))

    #calculation
    paint_gallons = total_sqft / SQFT_PER_GALLON
    paint_cost = paint_gallons * price_per_gallon
    labor_cost = total_sqft * painting_rate
    total_cost = paint_cost + labor_cost
    paint_percent = paint_cost / total_cost
    labor_percent = labor_cost / total_cost

    print("\nOutput")
    print("{0:20} {1:>9}".format("Item","Amount"))
    print("{0:20} {1:9,.2f}".format("Total Sq Ft:",total_sqft))
    print("{0:20} {1:9,.2f}".format("Sq Ft per Gallon:",SQFT_PER_GALLON))
    print("{0:20} {1:9,.2f}".format("Number of Gallons:",paint_gallons))
    print("{0:20} {1:8,.2f}".format("Paint per Gallon:",price_per_gallon))
    print("{0:20} {1:8,.2f}".format("Labor per Sq Ft:",painting_rate))

    print()

    print("{0:8} {1:>9} {2:>13}".format("Cost","Amount","Percentage"))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Paint:", paint_cost, paint_percent))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Labor:", labor_cost, labor_percent))
    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Total:", total_cost, paint_percent + labor_percent))