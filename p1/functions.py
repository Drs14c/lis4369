
def get_requirements():
    print("Data Analysis")
    print("\nProgram Requirements:\n"
        +"1. Run demo.py. \n"
        +"2. If errors, more than likely missing installations.\n"
        +"3. Test Python Package Installer: pip freeze\n"
        +"4. Research how to do the following installations:\n"
        +"\ta. pandas (only if missing)\n"
        +"\tb. pandas-datareader (only if missing)\n"
        +"\tc. pandas (only if missing)\n"
        +"5. Create at least three functions that are called by the program\n"
        +"\ta. main(): calls at least two other functions that are called by the program.\n"
        +"\tb. get_requirements(): displays the program requirements.\n"
        +"\tc. data_analysis(): displays the following data. \n")

def data_analysis():
    
    import pandas as pd #Pandas = 'python data analysis library'
    import datetime
    import pandas_datareader as pdr 
    import matplotlib.pylab as plt 
    from matplotlib import style

    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print("\nPrint columns: ")
    print(df.columns)
    
    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines: ")
    print(df.head())

    print("\nPrint last five lines")
    print(df.tail())

    print("\nPrint first 2 lines")
    print(df.head(2))

    print("\nPrint last 2 lines")
    print(df.tail(2))

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()